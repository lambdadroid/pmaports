# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdeconnect
_pkgname=kdeconnect-kde
pkgver=1.3.4
pkgrel=0
pkgdesc="Adds communication between KDE and your smartphone"
url="https://community.kde.org/KDEConnect"
arch="all"
license="GPL-2.0"
depends="kirigami2"
makedepends="extra-cmake-modules kdoctools-dev kconfigwidgets-dev kdbusaddons-dev kiconthemes-dev knotifications-dev kio-dev kcmutils-dev qca-dev plasma-framework-dev libexecinfo-dev qt5-qtdeclarative-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="$pkgname-$pkgver.tar.xz::https://download.kde.org/stable/$pkgname/$pkgver/$_pkgname-$pkgver.tar.xz"
options="!check" # Requires running X11 server
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	cd "$builddir"
	cmake \
		-DBUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_LIBEXECDIR=lib \
		-DEXPERIMENTALAPP_ENABLED=true \
		-DBUILD_TESTING=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	DESTDIR="$pkgdir" make install
}

sha512sums="286620cd5ed8da7a0f237710467f12be41ae139b14127b7049ee72c8a7042f67b599b642eb7c3e10a3504f1df11f5a9778db80fba4ac2e2fcd2ab4e8cafccfb9  kdeconnect-1.3.4.tar.xz"
